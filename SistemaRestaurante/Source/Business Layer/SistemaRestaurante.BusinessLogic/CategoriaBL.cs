﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SistemaRestaurante.BusinessEntity;
using SistemaRestaurante.Utils;
using SistemaRestaurante.DataAcess;

namespace SistemaRestaurante.BusinessLogic
{
    public class CategoriaBL : Singleton<CategoriaBL>
    {
        public IList<Categoria> Listar()
        {
            try
            {
                return CategoriaDAL.Instancia.Listar();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int Agregar (Categoria categoria)
        {
            try
            {
                return CategoriaDAL.Instancia.Agregar(categoria);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Modificar (Categoria categoria)
        {
            try
            {
                return CategoriaDAL.Instancia.Modificar(categoria);
            }
            catch (Exception ex) 
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Eliminar (int idCategoria)
        {
            try
            {
                return CategoriaDAL.Instancia.Eliminar(idCategoria);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Categoria Obtener (int idCategoria)
        {
            try
            {
                return CategoriaDAL.Instancia.Obtener(idCategoria);
            }
            catch(Exception ex )
            {
                
                throw new Exception(ex.Message);
            }
        }
    }
}
