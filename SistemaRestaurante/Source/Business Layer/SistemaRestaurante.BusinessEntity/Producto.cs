﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SistemaRestaurante.BusinessEntity
{
    public class Producto
    {
        public int IdProducto { set; get; }
        public string  Nombre { set; get; }
        public string Descripcion { set; get; }
        public string Imagen { set; get; }
        public int IdCegoria { set; get; }
        public decimal Precio { set; get; }
    }
}
