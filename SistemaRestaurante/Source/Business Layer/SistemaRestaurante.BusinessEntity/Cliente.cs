﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SistemaRestaurante.BusinessEntity
{
    public class Cliente
    {
        public int IdCliente { get; set; }
        public string Documento { get; set; }
        public string Nombre { get; set; }


    }
}
