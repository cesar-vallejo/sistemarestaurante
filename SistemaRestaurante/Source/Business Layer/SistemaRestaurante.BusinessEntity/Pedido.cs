﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SistemaRestaurante.BusinessEntity
{
    public class Pedido
    {
        public int IdPedido { set; get; }
        public int  IdCliente { set; get; }
        public int IdLocal { set; get; }
        public DateTime Fecha { set; get; }
        public int TipoPedido { set; get; }

    }
}
