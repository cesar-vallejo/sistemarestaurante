﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SistemaRestaurante.BusinessEntity
{
    public class DetallePedido
    {
        public int IdPedido { set; get; }
        public int IdItem { set; get; }
        public int IdProducto { set; get; }
        public int Cantidad { set; get; }
        public decimal Precio { set; get; }

    }
}
